A2J Viewer

To configure
=============
Once installed you need to:
* Download the A2J viewer library from GitHub and save in sites/all/libraries as a2j_viewer such that there is a path of /sites/all/libraries/a2j_viewer/viewer.
* Add a file field to whatever content type(s) you want to include embedded A2J interviews on.  The field should only accept zip files.  You can use any field name but the same field should be used in any content type that needs to support A2J interviews for embedded  viewing
* Go to the hosted A2J configuration menu item and
  1. Enter your field name (for example, field_a2j_interview)
  2. Enter a subdirectory name for where files should be stored (for example, guides)
  3. Enter a css class name if you want to override the default a2j class
* Copy the included field—field-a2j-interview.tpl.php file to your theme’s directory.  If you used a different field name other than field-a2j-interivew, rename the file to match field—[your field name].tpl.php

To use
=======
To use, just export your A2J interview as a zip from A2J author and upload through the field on your Drupal site.